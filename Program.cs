﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Class1.Services;

namespace Class1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pls enter your name: ");
            string incomingName = Console.ReadLine();

            Console.WriteLine("....");
            var userName = GetMyName(incomingName);

            var secondName = OutputClass.GetUserName();

            Console.WriteLine(userName);
            Console.WriteLine(secondName);
            Console.ReadLine();
        }

        static string GetMyName(string name)
        {
            string myName = "Gospel Chi";
            string message = $"This incoming name {name} is different from the manually typed one {myName}";

            return message;
        }
    }
}
